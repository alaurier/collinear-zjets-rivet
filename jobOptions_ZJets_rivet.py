theApp.SkipEvents = 0
#theApp.EvtMax = 100 

import os
import AthenaPoolCnvSvc.ReadAthenaPool
### Sherpa Zee DSID 364120,140-280 HTPTV Slice, CVetoBVeto, only 1 file
#svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/a/alaurier/private/ZJetsRivet/Zee364120.root"]
### Sherpa Zmumu DSID 364106,140-280 HTPTV Slice, CVetoBVeto, only 1 file
svcMgr.EventSelector.InputCollections = ["/afs/cern.ch/work/a/alaurier/private/ZJetsRivet/Zmm364106.root"]

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output = ["Rivet DATAFILE='ZJets_truth.root' OPT='RECREATE'"] 
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += ['ZJets_rivet']
# leave empty if unknown
#rivet.RunName = ""
rivet.OutputLevel = 0 
#rivet.OutputLevel = DEBUG 
#rivet.HistoFile = "ZJets_truth"
#rivet.DoRootHistos = True 
## SkipWeights turns off multiWeights! i.e multiple histograms!
#rivet.SkipWeights=True 
job += rivet
