// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"

namespace Rivet {

  /// Colinear Z + Jets in pp at 13 TeV
  class ZJets_rivet : public Analysis {
  public:

    Histo1DPtr _h_ZMass, _h_NJets_incl, _h_NJets_excl,_h_NJets_loDR, _h_NJets_hiDR, _h_j0pT, _h_clJetpT, _h_j1pT, _h_ZJpT, _h_ZJpT_loDR, _h_ZJpT_hiDR, _h_DR, _h_HT, _h_mjj, _h_mjj_loDR, _h_mjj_hiDR, _h_dPhi, _h_dPhi_loDR, _h_dPhi_hiDR, _h_ZpT_100GeVjet, _h_ZpT_500GeVjet;
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ZJets_rivet);

    /// Book histograms and initialise projections before the run
    void init() {

      // Define final state |eta| < 5
      const FinalState fs(Cuts::abseta < 5.0);

      IdentifiedFinalState photon_fs(fs);
      photon_fs.acceptIdPair(PID::PHOTON);
      PromptFinalState photons(photon_fs);

      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);

      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);

      // Kinematic cuts for leptons
      //Cut cuts_el = (Cuts::pT > 27*GeV) && ( Cuts::abseta < 1.37 || (Cuts::abseta > 1.52 && Cuts::abseta < 2.47) );
      Cut cuts_el = (Cuts::pT > 27*GeV) && (Cuts::abseta < 2.47);
      Cut cuts_mu = (Cuts::pT > 27*GeV) && (Cuts::abseta < 2.4);
      // Should I just create 1 cut without the crack?
      // Cut cuts = (Cuts::pT > 27*GeV) && (Cuts::abseta < 2.4);

      DressedLeptons dressed_electrons(photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      FastJets jets(fs, FastJets::ANTIKT, 0.4, JetAlg::NO_MUONS, JetAlg::NO_INVISIBLES);
      declare(jets, "Jets");

      _h_ZMass = bookHisto1D("ZMass" , 55, 50,160);

      _h_NJets_incl = bookHisto1D("NJets_incl" , 8, 1,9);
      _h_NJets_excl = bookHisto1D("NJets_excl" , 8, 1,9);
      _h_NJets_loDR = bookHisto1D("NJets_loDR" , 8, 1,9);
      _h_NJets_hiDR = bookHisto1D("NJets_hiDR" , 8, 1,9);

      _h_j0pT = bookHisto1D("j0pT",    std::vector<double>{70.,80.,110., 130., 150., 170., 190., 210., 250., 290., 350., 410., 510., 610., 710., 810., 1010.,1500});
      _h_clJetpT =bookHisto1D("clJetpT", std::vector<double>{70.,80.,110., 130., 150., 170., 190., 210., 250., 290., 350., 410., 510., 610., 710., 810., 1010.,1500});
      _h_j1pT = bookHisto1D("j1pT",    std::vector<double>{70.,80.,110., 130., 150., 170., 190., 210., 250., 290., 350., 410., 510., 610., 710., 810., 1010.,1500});

      _h_ZJpT = bookHisto1D("ZJpT"     , 30, 0, 3);
      _h_ZJpT_loDR = bookHisto1D("ZJpT_loDR", 30, 0, 3);
      _h_ZJpT_hiDR = bookHisto1D("ZJpT_hiDR", 30, 0, 3);

      _h_DR = bookHisto1D("DR", 40, 0, 4);
      _h_HT = bookHisto1D("HT", std::vector<double>{60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400., 500., 600., 700., 1000., 1500.});

      _h_mjj = bookHisto1D("mjj",      std::vector<double>{0., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 550., 600., 650., 700., 800., 1000., 1200., 1400., 1600., 2000.});
      _h_mjj_loDR = bookHisto1D("mjj_loDR", std::vector<double>{0., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 550., 600., 650., 700., 800., 1000., 1200., 1400., 1600., 2000.});
      _h_mjj_hiDR = bookHisto1D("mjj_hiDR", std::vector<double>{0., 50., 100., 150., 200., 250., 300., 350., 400., 450., 500., 550., 600., 650., 700., 800., 1000., 1200., 1400., 1600., 2000.});

      _h_dPhi = bookHisto1D("dPhijj",      std::vector<double>{0.0, 0.7854, 1.5708, 2.3562, 2.7489, 2.9452, 3.1416});
      _h_dPhi_loDR = bookHisto1D("dPhijj_loDR", std::vector<double>{0.0, 0.7854, 1.5708, 2.3562, 2.7489, 2.9452, 3.1416});
      _h_dPhi_hiDR = bookHisto1D("dPhijj_hiDR", std::vector<double>{0.0, 0.7854, 1.5708, 2.3562, 2.7489, 2.9452, 3.1416});

      _h_ZpT_100GeVjet = bookHisto1D("ZpT_100GeVjet", std::vector<double>{60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400., 500., 600., 700., 1000., 1500.});
      _h_ZpT_500GeVjet = bookHisto1D("ZpT_500GeVjet", std::vector<double>{60., 80., 100., 120., 140., 160., 180., 200., 240., 280., 340., 400., 500., 600., 700., 1000., 1500.});

    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // access fiducial electrons and muons
      const Particle *l1 = nullptr, *l2 = nullptr;
      auto muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      auto elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();

      // Dilepton selection
      if (muons.size()+elecs.size()!=2) vetoEvent;
      if      (muons.size()==2) { l1=&muons[0]; l2=&muons[1]; }
      else if (elecs.size()==2) { l1=&elecs[0]; l2=&elecs[1]; }
      else if (elecs.size()+muons.size()==2) { l1=&elecs[0]; l2=&muons[0]; }
      else vetoEvent;

      // Dilepton selection :: Z mass peak.
      // Also could do opposite charged if we go by single channel
      if ( inRange((l1->mom()+l2->mom()).mass()/GeV, 50.0, 160.0) )
        _h_ZMass->fill((l1->mom()+l2->mom()).mass()/GeV,event.weight());
      
      if ( !inRange((l1->mom()+l2->mom()).mass()/GeV, 71.0, 111.0) ) vetoEvent;
      //if (l1->threeCharge()+l2->threeCharge()!=0) vetoEvent;

      // lepton-jet overlap removal (note: muons are not included in jet finding)
      // Jets eta < 2.5, pT > 30GeV for overlap removal
      auto all_jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 30.0*GeV && Cuts::absrap < 2.5);
      // Remove all jets within dR < 0.2 of a dressed lepton
      idiscardIfAnyDeltaRLess(all_jets, muons, 0.2);
      idiscardIfAnyDeltaRLess(all_jets, elecs, 0.2);

      // make sure neither lepton overlaps with a jet within 0.4
      for (auto j: all_jets) {
        // Boosted Overlap Removal
        double dR = 0.04 + 10/(l1->pT()/GeV);
        if (dR > 0.4) dR = 0.4;
        if ( deltaR(j.mom(),l1->mom(),RAPIDITY)< dR)
          vetoEvent;
        dR = 0.04 + 10/(l2->pT()/GeV);
        if (dR > 0.4) dR = 0.4;
        if ( deltaR(j.mom(),l2->mom(),RAPIDITY)< dR)
          vetoEvent;
        //if ( deltaR(j.mom(),l1->mom(),RAPIDITY)<0.4 || deltaR(j.mom(),l2->mom(),RAPIDITY)<0.4 )
        //  vetoEvent;
      }

      // Calculate the observables
      double HT = 0.;
      double jet0pT = 0.;
      double cljetpT = 0.;
      double minDR = 99.;

      // Dilepton
      auto ll = (l1->mom() + l2->mom());
      double ZpT = ll.pT();

      // Require jets to be above 100GeV
      std::vector<Jet> Jets;
      for (auto j:all_jets) {
        if (j.pT()/GeV >= 100.0) {
          HT += j.pT();
          Jets.push_back(j);
        }
      }
      const size_t Njets = Jets.size();
      // Exclusive NJets, jet pT > 100 GeV
      _h_NJets_excl->fill(Njets,event.weight());
      // Inclusive NJets, jet pT > 100 GeV
      for(size_t i = 0; i <= Njets; ++i) {
        _h_NJets_incl->fill(i,event.weight());
      }

      // NJets >= 1
      if (Njets < 1) vetoEvent;

      // Z pT
      _h_ZpT_100GeVjet->fill(ZpT/GeV,event.weight());

      // HT
      HT += l1->mom().pT();
      HT += l2->mom().pT();
      _h_HT->fill(HT/GeV,event.weight());

      //Leading jet kinematics
      jet0pT = Jets[0].pT();
      _h_j0pT->fill(jet0pT/GeV,event.weight());

      // Our high pT phase-space
      if (jet0pT/GeV < 500.0) vetoEvent;

      // Z pT in phase space
      _h_ZpT_500GeVjet->fill(ZpT/GeV,event.weight());
      // Z/J pT ratio
      _h_ZJpT->fill(ZpT/jet0pT,event.weight());

      // Kinematics of 2 leading jets
      if (Njets > 1) {
        _h_j1pT->fill(Jets[1].pT()/GeV,event.weight());
        // Dijet mass
        _h_mjj->fill((Jets[0].mom()+Jets[1].mom()).mass()/GeV,event.weight());
        // Dijet delta Phi
        _h_dPhi->fill(deltaPhi(Jets[0].mom(), Jets[1].mom())/GeV,event.weight());
      }

      // closest jet to Z
      for (auto j : Jets) {
        if (deltaR( j.mom(), ll ,RAPIDITY) < minDR) {
          minDR = deltaR( j.mom(), ll ,RAPIDITY);
          cljetpT = j.pT();
        }
      }
      _h_clJetpT->fill(cljetpT/GeV,event.weight());
      _h_DR->fill(minDR,event.weight());


      // Phase space with DR<2.0
      if (minDR < 2.0) {
        if (Njets > 1) {
          _h_mjj_loDR->fill((Jets[0].mom()+Jets[1].mom()).mass()/GeV,event.weight());
          _h_dPhi_loDR->fill(deltaPhi(Jets[0].mom(), Jets[1].mom())/GeV,event.weight());
        }
        _h_NJets_loDR->fill(Njets,event.weight());
        _h_ZJpT_loDR->fill(ZpT/jet0pT,event.weight());
      } else {
        if (Njets > 1) {
          _h_mjj_hiDR->fill((Jets[0].mom()+Jets[1].mom()).mass()/GeV,event.weight());
          _h_dPhi_hiDR->fill(deltaPhi(Jets[0].mom(), Jets[1].mom())/GeV,event.weight());
        }
        _h_NJets_hiDR->fill(Njets,event.weight());
        _h_ZJpT_hiDR->fill(ZpT/jet0pT,event.weight());
      }

    }

    void finalize() {
      double xsec = crossSectionPerEvent();
      // According to Old Z+Jets paper, need to divide by 2 when dealing with both channels at once.
      // Need to verify this.
      // xsec = xsec * 0.5;

      // Normalize to cross section.
      scale(_h_ZMass,xsec);
      scale(_h_NJets_incl,xsec);
      scale(_h_NJets_excl,xsec);
      scale(_h_NJets_loDR,xsec);
      scale(_h_NJets_hiDR,xsec);
      scale(_h_j0pT,xsec);
      scale(_h_clJetpT,xsec);
      scale(_h_j1pT,xsec);
      scale(_h_ZJpT,xsec);
      scale(_h_ZJpT_loDR,xsec);
      scale(_h_ZJpT_hiDR,xsec);
      scale(_h_DR,xsec);
      scale(_h_HT,xsec);
      scale(_h_mjj,xsec);
      scale(_h_mjj_loDR,xsec);
      scale(_h_mjj_hiDR,xsec);
      scale(_h_dPhi,xsec);
      scale(_h_dPhi_loDR,xsec);
      scale(_h_dPhi_hiDR,xsec);
      scale(_h_ZpT_100GeVjet,xsec);
      scale(_h_ZpT_500GeVjet,xsec);
    } // end of finalize

  };

  DECLARE_RIVET_PLUGIN(ZJets_rivet);
}
