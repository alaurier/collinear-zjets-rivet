# BEGIN PLOT /ZJets_rivet/..
XTwosidedTicks=1
YTwosidedticks=1
LeftMargin=1.5
XMinorTickMarks=0
LogY=1
LegendAlign=r
Title=$Z \rightarrow \ell^+ \ell^-$, dressed level
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZMass
LogY=1
XLabel=$M_{\ell \ell}$ [GeV]
#YLabel=Events
# END PLOT

# BEGIN PLOT /ZJets_rivet/NJets_incl
LogY=1
XLabel=$N_\text{jets}$ inclusive
XCustomMajorTicks = 1 $\ge$1 2 $\ge$2 3 $\ge$3 4 $\ge$4 5 $\ge$5 6 $\ge$6 7 $\ge$7 8 $\ge$8 
#LegendYPos=0.3
#YLabel=Events
# END PLOT

# BEGIN PLOT /ZJets_rivet/NJets_excl
LogY=1
XLabel=$N_\text{jets}$ exclusive
#LegendYPos=0.3
#YLabel=Events
# END PLOT

# BEGIN PLOT /ZJets_rivet/NJets_loDR
LogY=1
XLabel=$N_\text{jets}$ minDR(Z,J)$\le\text{2.0}$
#LegendYPos=0.3
#YLabel=Events
# END PLOT

# BEGIN PLOT /ZJets_rivet/NJets_hiDR
LogY=1
XLabel=$N_\text{jets}$ minDR(Z,J)$\ge\text{2.0}$
#LegendYPos=0.3
#YLabel=Events
# END PLOT

# BEGIN PLOT /ZJets_rivet/j0pT
LogY=1
XLabel=$p_\text{T}^\text{jet}$ (leading jet, $\ge$ 1 jet) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/j1pT
LogY=1
XLabel=$p_\text{T}^\text{jet}$ (second leading jet, $\ge$ 1 jet) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/clJetpT
LogY=1
XLabel=$p_\text{T}^\text{jet}$ (closest jet to $Z$, $\ge$ 1 jet) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZpT_100GeVjet
LogY=1
XLabel=$p_\text{T}^\text{Z}$ ($p_\text{T}$ leading jet $\ge$ 100GeV) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZpT_500GeVjet
LogY=1
XLabel=$p_\text{T}^\text{Z}$ ($p_\text{T}$ leading jet $\ge$ 500GeV) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZJpT
LogY=0
XLabel=$\frac{p_\text{T}\text{Z}}{p_\text{T}\text{jet}}$ (leading jet $p_\text{T} \ge$ 500 GeV)
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZJpT_loDR
LogY=0
XLabel=$\frac{p_\text{T}\text{Z}}{p_\text{T}\text{jet}}$ (minDR(Z,J)$\le\text{2.0}$)
# END PLOT

# BEGIN PLOT /ZJets_rivet/ZJpT_hiDR
LogY=0
XLabel=$\frac{p_\text{T}\text{Z}}{p_\text{T}\text{jet}}$ (minDR(Z,J)$\ge\text{2.0}$)
# END PLOT

# BEGIN PLOT /ZJets_rivet/DR
LogY=0
XLabel=minDR(Z,J) (leading jet $p_\text{T} \ge$ 500GeV)
# END PLOT

# BEGIN PLOT /ZJets_rivet/HT
LogY=1
XLabel=$H_\text{T}$  ($N_\text{jets}\ge$1) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/mjj
LogY=1
XLabel=$M_\text{jj}$  (2 leading jets) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/mjj_loDR
LogY=1
XLabel=$M_\text{jj}$  (2 leading jets, minDR(Z,J)$\le\text{2.0}$) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/mjj_hiDR
LogY=1
XLabel=$M_\text{jj}$  (2 leading jets, minDR(Z,J)$\ge\text{2.0}$) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/dPhijj
LogY=1
XLabel=$\Delta\phi_\text{jj}$  (2 leading jets)
# END PLOT

# BEGIN PLOT /ZJets_rivet/dPhijj_loDR
LogY=1
XLabel=$\Delta\phi_\text{jj}$  (2 leading jets, minDR(Z,J)$\le\text{2.0}$) [GeV]
# END PLOT

# BEGIN PLOT /ZJets_rivet/dPhijj_hiDR
LogY=1
XLabel=$\Delta\phi_\text{jj}$  (2 leading jets, minDR(Z,J)$\ge\text{2.0}$) [GeV]
# END PLOT
