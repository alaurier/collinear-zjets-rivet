// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"

namespace Rivet {

  /// Colinear Z + Jets in pp at 13 TeV
  class ZJets_rivet : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(ZJets_rivet);

    /// Book histograms and initialise projections before the run
    void init() {

      // AntiKt4TruthWZJets prescription
      // Photons
      FinalState photons(Cuts::abspid == PID::PHOTON);
      
      // Muons
      PromptFinalState bare_mu(Cuts::abspid == PID::MUON, true); // true = use muons from prompt tau decays
      DressedLeptons all_dressed_mu(photons, bare_mu, 0.1, Cuts::abseta < 2.5, true);
      
      // Electrons
      PromptFinalState bare_el(Cuts::abspid == PID::ELECTRON, true); // true = use electrons from prompt tau decays
      DressedLeptons all_dressed_el(photons, bare_el, 0.1, Cuts::abseta < 2.5, true);
      
      //Jet forming
      VetoedFinalState vfs(FinalState(Cuts::abseta < 4.5));
      vfs.addVetoOnThisFinalState(all_dressed_el);
      vfs.addVetoOnThisFinalState(all_dressed_mu);
            
      FastJets jet(vfs, FastJets::ANTIKT, 0.4, JetAlg::Muons::ALL, JetAlg::Invisibles::NONE);
      declare(jet, "Jets");


      // Current definition of leptons + jets
      // Define final state |eta| < 4.5
      const FinalState fs(Cuts::abseta < 4.5);

      IdentifiedFinalState photon_fs(fs);
      photon_fs.acceptIdPair(PID::PHOTON);
      PromptFinalState Photons(photon_fs);

      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);

      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);

      // Kinematic cuts for leptons
      Cut cuts_el = (Cuts::pT > 25*GeV) && (Cuts::abseta < 2.5);
      Cut cuts_mu = (Cuts::pT > 25*GeV) && (Cuts::abseta < 2.5);


      DressedLeptons dressed_electrons(Photons, electrons, 0.1, cuts_el);
      declare(dressed_electrons, "DressedElectrons");

      DressedLeptons dressed_muons(Photons, muons, 0.1, cuts_mu);
      declare(dressed_muons, "DressedMuons");

      book(_h["NJets_excl"],"NJets_excl" , 7, 0.5,7.5);
      book(_h["NJets500_excl"],"NJets500_excl" , 7, 0.5,7.5);
      book(_h["NJets_loDR"],"NJets_loDR" , 7, 0.5,7.5);
      book(_h["NJets_hiDR"],"NJets_hiDR" , 7, 0.5,7.5);
      // HTjet > 600GeV variables
      book(_h["NJetsHT600_excl"],"NJetsHT600_excl" , 7, 0.5,7.5);
      book(_h["DR_HT600"], "DR_HT600", std::vector<double>{0.0, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 4.0});

      book(_h["ZpT_100GeVjet"], "ZpT_100GeVjet", std::vector<double>{0, 20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 240, 280, 340, 400, 500, 600, 700, 860, 1500, 3000});
      book(_h["leadingJet_pT"], "j0pT",    std::vector<double>{100, 140, 180, 220, 260, 300, 360, 420, 520, 620, 720, 820, 1020, 1220, 2000});

      book(_h["ZJ_pTRatio"],	  "ZJpT"     , std::vector<double>{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.2, 1.5});
      book(_h["ZJ_pTRatio_loDR"], "ZJpT_loDR", std::vector<double>{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0, 1.2, 1.5});
      book(_h["ZJ_pTRatio_hiDR"], "ZJpT_hiDR", std::vector<double>{0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.5});

      book(_h["DR"], "DR", std::vector<double>{0.0, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 4.0});
      book(_h["HT"], "HT", std::vector<double>{150, 200, 250, 300, 400, 500, 600, 700, 800, 1000, 1200, 1400, 1600, 2000, 4000});
      
    }

    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Abnormally large Event weight in Sherpa2.2.1
      //if (std::abs(event.weights()[0]) > 100.) vetoEvent;


      // access fiducial electrons and muons
      const Particle *l1 = nullptr, *l2 = nullptr;
      auto muons = apply<DressedLeptons>(event, "DressedMuons").dressedLeptons();
      auto elecs = apply<DressedLeptons>(event, "DressedElectrons").dressedLeptons();

      // lepton-jet overlap removal (note: muons are not included in jet finding)
      // Jets eta < 2.5, pT > 30GeV for overlap removal
      auto all_jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 30.0*GeV && Cuts::absrap < 2.5);
      // Remove all jets within dR < 0.2 of a dressed lepton
      idiscardIfAnyDeltaRLess(all_jets, muons, 0.2);
      idiscardIfAnyDeltaRLess(all_jets, elecs, 0.2);
      
      // Remove leptons within dR < 0.4 of a jet
      idiscardIfAnyDeltaRLess(muons, all_jets, 0.4);
      idiscardIfAnyDeltaRLess(elecs, all_jets, 0.4);

      if (elecs.size() + muons.size() !=2) vetoEvent;

      if (elecs.size()==2){
        l1=&elecs[0];
        l2=&elecs[1];
      }
      else if (muons.size()==2){
        l1=&muons[0];
        l2=&muons[1];
      }
      else vetoEvent;

      // if not e+e- or mu+mu- pair, veto
      if (l1->pid() + l2->pid() !=0) vetoEvent;

      // Dilepton selection :: Z mass peak.
      auto ll = (l1->mom() + l2->mom());
      double Zm  = ll.mass(); 
      if ( !inRange(Zm/GeV, 71.0, 111.0) ) vetoEvent;
      double ZpT = ll.pT();
      
      // Calculate the observables
      double HT = 0.;
      double HTjet = 0.;
      double jet0pT = 0.;
      double cljetpT = 0.;
      double minDR = 99.;

      // Require jets to be above 100GeV
      std::vector<Jet> Jets;
      for (auto j:all_jets) {
        if (j.pT()/GeV >= 100.0) {
          HT += j.pT();
          HTjet += j.pT();
          Jets.push_back(j);
        }
      }
      const size_t Njets = Jets.size();

      // NJets >= 1      
      if (Njets < 1) vetoEvent;
      // Exclusive NJets, jet pT > 100 GeV

      _h["NJets_excl"]->fill(Njets);

      // Z pT
      _h["ZpT_100GeVjet"]->fill(ZpT/GeV);
      
      //Leading jet pT 
      jet0pT = Jets[0].pT();
      _h["leadingJet_pT"]->fill(jet0pT/GeV);

      // HT
      HT += (l1->pT() + l2->pT());
      _h["HT"]->fill(HT/GeV);

      // HTJet > 600 GeV selection
      if (HTjet >= 600.){
        double minDR_HTjet = 99.;
        // closest jet to Z
        for (auto j : Jets) {
          if (deltaR( j.mom(), ll ,RAPIDITY) < minDR_HTjet) {
            minDR_HTjet = deltaR( j.mom(), ll ,RAPIDITY);
          }
        }
        // Fill histograms of HTjet > 600 GeV
        _h["NJetsHT600_excl"]->fill(Njets);
        _h["DR_HT600"]->fill(minDR_HTjet);
      } // end of HTjet > 600 GeV

      // Our high pT phase-space
      if (jet0pT/GeV < 500.0) vetoEvent;
      
      // Exclusive NJets, jet pT > 500 GeV
      _h["NJets500_excl"]->fill(Njets);
      
      // Z/J pT ratio

      // closest jet to Z
      for (auto j : Jets) {
        if (deltaR( j.mom(), ll ,RAPIDITY) < minDR) {
          minDR = deltaR( j.mom(), ll ,RAPIDITY);
          cljetpT = j.pT();
        }
      }
      // Z/J pT ratio
      _h["ZJ_pTRatio"]->fill(ZpT/cljetpT);
      _h["DR"]->fill(minDR);
      
      // Phase space with DR<1.4       
      if (minDR < 1.4) {
        _h["NJets_loDR"]->fill(Njets);
        _h["ZJ_pTRatio_loDR"]->fill(ZpT/cljetpT);
      // Phase space with DR>2.0
      } else if (minDR >2.0) {
        _h["NJets_hiDR"]->fill(Njets);
        _h["ZJ_pTRatio_hiDR"]->fill(ZpT/cljetpT);
      }

    }

    void finalize() {
      double xsec = crossSection()/picobarn/sumW();
      // According to Old Z+Jets paper, need to divide by 2 when dealing with both channels at once.
      // Need to verify this.
      // xsec = xsec * 0.5;

      // Normalize to cross section.
      // Inclusive region
      scale(_h["NJets_excl"],xsec);
      scale(_h["HT"],xsec);
      scale(_h["ZpT_100GeVjet"],xsec);
      scale(_h["leadingJet_pT"],xsec);
      // High pT region (pT jet > 500 GeV)
      scale(_h["NJets500_excl"],xsec);
      scale(_h["ZJ_pTRatio"],xsec);
      scale(_h["DR"],xsec);
      // High pT + collinear (minDR < 1.4)
      scale(_h["NJets_loDR"],xsec);
      scale(_h["ZJ_pTRatio_loDR"],xsec);
      scale(_h["NJets_hiDR"],xsec);
      // High pT + back-to-back (minDR > 2.0)
      scale(_h["ZJ_pTRatio_hiDR"],xsec);
      // HTjet > 600 GeV
      scale(_h["NJetsHT600_excl"],xsec);
      scale(_h["DR_HT600"],xsec);

    } // end of finalize

    //@}

    // define histograms
    map<string, Histo1DPtr> _h;
 
  };

  DECLARE_RIVET_PLUGIN(ZJets_rivet);
}
